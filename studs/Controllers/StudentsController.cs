﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using studs.Models;
using studs.Repository;

namespace studs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentRepository _studentRepo;
        public StudentsController(IStudentRepository studentRepository)
        {
            _studentRepo= studentRepository ?? throw new ArgumentNullException(nameof(studentRepository));
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Student> Get()
        {
            return _studentRepo.GetStudents();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Student Get(int id)
        {
            return _studentRepo.GetStudent(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] Student student)
        {
            _studentRepo.AddStudent(student);
        }

       
        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(Student student)
        {
            _studentRepo.RemoveStudent(student);
        }
    }
}
