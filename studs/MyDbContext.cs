﻿using Microsoft.EntityFrameworkCore;
using studs.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace studs
{
    public class MyDbContext:DbContext
    {
        public MyDbContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<Student> Students;
        public DbSet<Course> Courses;
        public DbSet<CourseDetail> CourseDetails;

        //for foreign keys
        protected override void OnModelCreating(ModelBuilder model)
        {
            model.Entity<CourseDetail>().HasKey(c => new { c.CourseId, c.StudentId });
        }
    }
}
