﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using studs.Models;

namespace studs.Repository
{
    public class StudentRepository : IStudentRepository
    {
        private readonly MyDbContext _context;
        private readonly DbSet<Student> students;

        public StudentRepository(MyDbContext context)
        {
            _context=context??throw new ArgumentNullException(nameof(context));
            students = context.Set<Student>();
        }
        public void AddStudent(Student student)
        {
            students.Add(student);
            _context.SaveChanges();
        }

        public Student GetStudent(int id)
        {
            return students.Include(c => c.Courses).FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<Student> GetStudents()
        {
            return students.Include(c => c.Courses);
        }

        public void RemoveStudent(Student student)
        {
            students.Remove(student);
            _context.SaveChanges();
        }

        
    }
}
